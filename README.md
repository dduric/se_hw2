#KALKULATOR
##by Dario Đurić

**upute**
Kalkulator radi isključivo na klik

Brojevi i matematička operacija unose se zajedno u input polje, te se klikom na tipku "=" izvrši unesena matematička operacija

Tipke CE i C imaju istu funkciju, brišu trenutni sadržaj input polja

Tipka DEL briše posljednji broj u input polju

Tipka (-) mijenja predznak broja prije matematičke operacije