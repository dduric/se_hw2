import kivy
kivy.require('1.7.0')

from kivy.app import App
from kivy.uix.gridlayout import GridLayout

class MyLayout(GridLayout):

    def calculate(self, calculation):
        if calculation:
            try:
                self.display.text = str(eval(calculation))
            except Exception:
                self.display.text = "Error"

    def negative(self, *args):
        calc_input = self.ids.input.text
        if calc_input != '':
            if calc_input[0] == '-':
                self.ids.input.text = calc_input[1:]
            else:
                self.ids.input.text = '-' + calc_input

class CalculatorApp(App):
    def build(self):
        return MyLayout()

if __name__ == "__main__":
    CalculatorApp().run()
